package net.acaproni.sudoku

/**
 * The field. It is composed of rows, columns and squares with their rules
 * 
 * @param row The rows of the Field
 */
class Field(val rows: Vector[SRow]) {
  require(Option(rows).isDefined)
  require(rows.size==SRow.rowSize,s"Invalid number of rows ${rows.size}")
  
  // Columns are like rows but displaced vertically: they have the same rules
  val cols: Vector[SCol] = {
    val arrayOfCols: Array[SCol] = new Array(9)
   
    for(c <- 0 to 8) {
      val values: List[Option[Int]] = (0 to 8).foldLeft(List.empty[Option[Int]])( (l,i) => rows(i).get(c)::l).reverse
      arrayOfCols(c) = new SCol(values.toVector)
    }
    arrayOfCols.toVector
  }
  
  // The 9 squares in the field with the same basic rules of rows and cols
  // the first square (0) is at the top left, the latest (8) at the bottom right
  val squares: Vector[SSquare] = {
    val arrayOfSquares: Array[SSquare] = new Array(9)
    
    for (
        r <- 0 to 6 by 3;
        c <- 0 to 6 by 3;
        pos=r+c/3) {
      
      val row1 = Vector(get(r,c),get(r,c+1),get(r,c+2))
      val row2 = Vector(get(r+1,c),get(r+1,c+1),get(r+1,c+2))
      val row3 = Vector(get(r+2,c),get(r+2,c+1),get(r+2,c+2))
      
      val rows: Vector[Vector[Option[Int]]] = Vector(row1,row2,row3)
      
      arrayOfSquares(pos)= SSquare(rows)
    }
    arrayOfSquares.toVector
  }
  
  /**
   * True if there is at least one duplicate in the field, false otherwise
   */
  def containsDuplicates: Boolean = {
    rows.exists(_.containsDuplicates) ||
    cols.exists(_.containsDuplicates) ||
    squares.exists(_.containsDuplicates)
  }
  
  /**
   * @param row The row [0,8]
   * @param col The column [0,8]
   * @return the value of the cell at the given col and row
   */
  def get(row: Int, col: Int): Option[Int] = {
    require(col>=0 && col<SRow.rowSize,s"Invalid col $col")
    require(row>=0 && row<SRow.rowSize,s"Invalid row $row")
    rows(row).get(col)
  }
  
  /**
   * Return a new field with the value set in the passed row and col
   * 
   * @param row The row [0,8]
   * @param col The column [0,8]
   * @param value The value to set in the col and row
   * @return A new field with the value in the passed row and col
   */
  def set(row: Int, col: Int, value: Option[Int]): Field = {
    require(col>=0 && col<SRow.rowSize,s"Invalid col $col")
    require(row>=0 && row<SRow.rowSize,s"Invalid row $row")
    if (value.isDefined) 
      require(value.get>=SRow.minAllowedValue && value.get<=SRow.maxAllowedValue,s"Value $value out of range")
    
    val newRow = rows(row).set(value, col)
    val newRows= for (
        t <- 0 until SRow.rowSize;
      x = if (t==row) newRow else rows(t)) yield(x) 
      
      new Field(newRows.toVector)
  }
  
  /**
   * Check if assigning the passed value to the cell is allowed 
   * i.e. does not introduce duplicates
   * 
   * @param row The row [0,8]
   * @param col The column [0,8]
   * @param value The value to check
   * @return true if setting the value in the passed cell. isAllowed; 
   *         false if introduces duplicates
   */
  def isAllowed(row: Int, col: Int, value: Int): Boolean = set(row, col, Some(value)).containsDuplicates
    
  /** 
   *  True if all the numbers are in the field; false otherwise
   */
  def isComplete: Boolean = rows.forall(_.isComplete())
  
  override def toString() = {
    val ret = new StringBuilder
    for (r <- 0 to 8) {
      ret.append("[ ")
      for (c <- 0 to 8) {
        val v = rows(r).get(c)
        ret.append(v.getOrElse("-"))
        ret.append(' ')
      }
      ret.append("]\n")
    }
    ret.toString()
  }
}

object Field {
  
  /**
   * @param row The row of the cell
   * @param col The column of the cell
   * @return the number of the square that contains the cell in the row and col
   */
  def getSquareOfCell(row: Int, col: Int): Int = {
    require(Option(row).isDefined)
    require(row>=0 && row<SRow.rowSize,s"Invalid row $row")
    require(Option(col).isDefined)
    require(col>=0 && col<SRow.rowSize,s"Invalid col $col")
    
    val posX=col/3
    val posY=row/3
    posX+3*posY    
  }
  
  /**
   * Build a empty field (no values set in any of the cells)
   */
  def apply(): Field = {
    val emptyRow: SRow = SRow()
    
    val array: Array[SRow] = new Array(9)
    for (i <- 0 to 8) array(i)=emptyRow
    new Field(array.toVector)
  }
}
