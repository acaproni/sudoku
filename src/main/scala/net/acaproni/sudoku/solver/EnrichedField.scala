package net.acaproni.sudoku.solver

import net.acaproni.sudoku.SRow
import net.acaproni.sudoku.Field
import scala.collection.immutable.VectorBuilder

/** 
 *  The EnrichedField is a Field where all the empty cells. (i.e. those having a value of Option.empty in the Field)
 *  are filled with a list of values that can be set in the cell without introducing duplicates.
 *  These are the only possible values that can be set in the cell to solve the game without violating
 *  the rules.
 *  
 *  If the list of possible values of a cell contains one and only one value then this value
 *  can be safely set in the cell.
 *  
 *  The EnrichedField is immutable
 *  
 *  @param rows The rows of the Field
 */
class EnrichedField(rows: Vector[SRow]) extends Field(rows) {
  
  /** 
   *  The list of values that can be set in a cell without breaking the rule.
   *  
   *  If the list contains one and only one value then this value can be set in the cell
   *  If the list is empty then the value has already been set in the Field and there is no other possible value
   *  
   *  This is the first and most basic completion technic that consists in cross checking 
   *  if a value can be put in a cell without breaking the rule. In general more values are possible in a cell
   *  but sometimes only one value is possible and in this case we know that we can set this value in the field and
   *  look for another value to set.
   *  Normally, when one value is set, this scan must be repeated until the game finishes or
   *  there are no more cells with only one possible case.
   *  In the latter, more sophisticated heuristics must be applied by the user or the machine. 
   *  The machine can always solve the game by recursively exploring all the possible values in the cells
   *  until the game is resolved.
   *    
   */
  val allowedValuesOnCells: Vector[Vector[List[Int]]] = {
    val allowedValues: Array[Array[List[Int]]] = new Array(SRow.rowSize)
    for (t <- 0 until SRow.rowSize) allowedValues(t)=new Array(SRow.rowSize)
    
    // Gets the possible values of the cell in the give row and col
    def possibleValuesOfCell(row: Int, col: Int): List[Int] = {
      if (get(row,col).isDefined) List.empty[Int]
      else {
        // The values not yet set in the row
        val possibleValuesInRow = rows(row).numsNotSet.toSet
        // The values not yet set in the col
        val possibleValuesInCol = cols(col).numsNotSet.toSet
        // The values not yet set in the square
        val square = Field.getSquareOfCell(row, col)
        val possibleValuesInSquare = squares(square).numsNotSet.toSet
        val ret=possibleValuesInRow.intersect(possibleValuesInCol).intersect(possibleValuesInSquare).toList.sorted
        ret
      }
    }
    
    // Iterates over the rows and cells
    for (
        x <- 0 until SRow.rowSize;
        y <- 0 until SRow.rowSize;
        possibleValues = possibleValuesOfCell(x,y)        
        ) {
      allowedValues(x)(y)=possibleValues
    }
    // Convertes the array of array (mutable) to a vector of vector (immutable)
    val temp: Array[Vector[List[Int]]] = new Array(SRow.rowSize)
    for (t <- 0 until SRow.rowSize) temp(t) = allowedValues(t).toVector
    temp.toVector
  }
  
  /**
   * The list of the values that can be safely set in the cells without violting the rules
   * 
   * These are the values that in the allowed values of a cell are unique 
   * (i.e. the list contains one and one one digit)
   */
  val fixableValues: Set[Int] = {
    val flattenVector = allowedValuesOnCells.flatten
    flattenVector.foldLeft( Set.empty[Int])( (s,v) => {
      if (v.size==1) s++Set(v.head)
      else s
    })
  }
  
  /**
   * True if there are cells with only one possible value to be inserted without breaking the rules;
   * false otherwise.
   * 
   * If the value is True then at least one empty cell can be filled with a value
   * and it is possible to make a step forward the completion of the game.
   * If no such cell then some more complex euristic must be implemented to solve the game
   */
  val containsSolvableCells: Boolean = !fixableValues.isEmpty
  
  
  
  /**
   * @return a String with the values of the field and the possible value of each cell
   */
  override def toString(): String = {
    val str = new StringBuilder
    for (x <- 0 until SRow.rowSize) {
      // New Row
      str.append("[ ")
      for (y <- 0 until SRow.rowSize) {
        str.append(get(x,y).getOrElse(allowedValuesOnCells(x)(y).toString()))
        str.append(' ')
      }
      str.append("]\n")
    }
    str.toString()
  }
}