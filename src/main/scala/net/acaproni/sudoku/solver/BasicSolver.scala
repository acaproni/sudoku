package net.acaproni.sudoku.solver

import net.acaproni.sudoku.Field
import net.acaproni.sudoku.SRow

/**
 * Solve the passed Sudoku. by applying the basi rule: unique value in cell, row and square
 * 
 * @param initial The sudoku to solve
 */
class BasicSolver(val initial: Field) {
  require(Option(initial).isDefined)
  
  /**
   * Look for unique values in the field that can be set in the cells, if any.
   * This is the most basic step to solve the game based on the presence of a digit in a row, col and square.
   * Normally this is the first step looking for the solution and it is repeated evey time that a 
   * digit is set in a cell
   * 
   * @return the field with the unique values set in the cell 
   *         or empty if there are no unique values that can be set applying this rule
   */
  def solve(): Option[Field] = {
    val enrichedField = new EnrichedField(initial.rows)
    println(s"Initial enrichedField:\n $enrichedField")
    println(s"Contains solvable cells ${enrichedField.containsSolvableCells}, fixableValues = ${enrichedField.fixableValues}")
    
    if (enrichedField.containsSolvableCells) {
      println(s"Fixing values ${enrichedField.fixableValues}")
      
      val rows: Array[SRow] = enrichedField.rows.toArray
      enrichedField.fixableValues.foreach( v => {
        println(s"Fixing $v")
        for (r <- 0 until SRow.rowSize) {
          val pos = enrichedField.allowedValuesOnCells(r).indexOf(List(v))
          if (pos>=0) {
            assert(!enrichedField.rows(r).isSet(pos))
            println(s"Setting $v in row($r): ${rows(r)}")
            rows(r)=rows(r).set(v, pos)
            println(s"New row ${rows(r)}")
          }
        }
      })
      Some(new Field(rows.toVector))  
    } else {
      println("Does not contains solvable cells: nothing to do")
      None
    }
  }
}