package net.acaproni.sudoku

/**
 * The 3x3 square: 9 of this squares are in the filed.
 * 
 * Beside being squares they follow the same rules of rows and columns
 * 
 * A square is, in fact a row, with some specialized method
 */
class SSquare(vals: Vector[Option[Int]]) extends SRow(vals) {
  
  val rowsOfSquare: Vector[Vector[Option[Int]]] =
    Vector( Vector(vals(0),vals(1),vals(2)), Vector(vals(3),vals(4),vals(5)), Vector(vals(6),vals(7),vals(8)))
    
  /**
   * @return the value in the given coln and row
   */
  def get(row: Int, col: Int): Option[Int] = {
    require(row>=0 && row<3, s"Invalid row $row")
    require(col>=0 && col<3, s"Invalid col $col")
    rowsOfSquare(row)(col)
  }
  
  override def toString() = {
    val ret = new StringBuilder
    
    for (r <- 0 to 2) {
      ret.append("[ ")
      for (c <- 0 to 2) {
        ret.append(get(r,c).getOrElse('-'))
        ret.append(' ')
      }
      ret.append("]\n")
    }
    ret.toString()
  }
}

object SSquare {
  
  /**
   * Builds a square composed of 3 rows each of which has 3 colums
   */
  def apply(rows: Vector[Vector[Option[Int]]]): SSquare = {
    require(Option(rows).isDefined && !rows.isEmpty,"Invalid empty rows")
    require(rows.size==3, s"Invalid number of rows ${rows.size}")
    require(!rows.exists(_.isEmpty), "Invalid empty row")
    require(!rows.exists(_.size!=3), "Invalid size of row")
    
    val vals: Array[Option[Int]] = new Array(SRow.rowSize)
    for {
      r <- 0 to 2
      c <- 0 to 2
    } {
      val value: Option[Int] =rows(r)(c)
      vals(3*r+c)=value
    }
    new SSquare(vals.toVector)
  }
}