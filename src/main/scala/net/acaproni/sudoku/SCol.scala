package net.acaproni.sudoku

/**
 * A columns is like a row with only difference in the way it is printed
 * 
 *  @param vals: the 9 values of the column
 */
class SCol(vals: Vector[Option[Int]]) extends SRow(vals) {
  
  override def toString(): String = {
    val temp = vals.map( v => {
      v match {
        case None => "-"
        case Some(n) => ""+n
      }
    })
    val ret = new StringBuilder
    
    for (i <- 0 to 8) {
      ret.append('[')
      ret.append(temp(i))
      ret.append(']')
      ret.append('\n')
    }
    ret.toString()
  }
}