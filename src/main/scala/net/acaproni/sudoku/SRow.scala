package net.acaproni.sudoku

import scala.collection.immutable.VectorBuilder

/**
 * A row in the sudoku field
 * 
 * Each cell of the row can or cannot contain a value.
 * 
 * The row is malformed if it contains at least one number is 2 different positions.
 * 
 * @param vals: the 9 values of the row 
 */
class SRow(val vals: Vector[Option[Int]]) {
  require(Option(vals).isDefined)
  require(vals.size==SRow.rowSize,s"Wrong size ${vals.size}")
  
  /** 
   *  The numbers that are set in the vector
   *  It can contain duplicates if the row is not correct
   */
  val numsSet: List[Int] = vals.foldLeft(List.empty[Int])( (l,v) => {
    v match {
      case Some(n) => n::l
      case None => l
    }
  }).sorted
  
  /** 
   *  The numbers that should go in the row to complete it
   *  The position of such number is not yet known.
   *  
   *  If the row is complete, this list empty
   */
  lazy val numsNotSet: List[Int] = {
    (1 to 9).foldLeft( List.empty[Int])( (l,n) => {
      if (numsSet.contains(n)) l
      else n::l
    })
  }.sorted
  
  /**
   * True if the row contains at least one value repeated twice
   * that means that the row is invalid
   */
  lazy val containsDuplicates: Boolean = numsSet.exists( value => numsSet.count(value==_)>1)
  
  /**
   * Gets and return the position where the given value is present in the row.
   * Normally the value is contained 0 or 1 times otherwise there is an error 
   * in the line
   * 
   * @return the positions where the value is present in the row; 
   *         empty if the row does not contains the value
   */
  def positionsOfValue(value: Int): List[Int] = {
    require(value>=SRow.minAllowedValue && value<=SRow.maxAllowedValue,s"Invalid value $value")
    // Iterate over the vals to look for duplicates 
    (0 until 9).foldLeft( List.empty[Int])( (l,pos) => {
      vals(pos) match {
        case None => l
        case Some(v) => if (value==v) pos::l else l
      }
    }).sorted
  }
  
  /** True if all the values of the row has been defined */
  def isComplete(): Boolean = numsNotSet.isEmpty
  
  /** Check if the row contains a value in the passed position */
  def isSet(pos: Int): Boolean = {
    require(pos>=0 && pos<=SRow.rowSize,s"Invalid position $pos")
    vals(pos).isDefined
  }
  
  def isUnset(pos: Int): Boolean = !isSet(pos)
  
  /**
   * Return a new row where the passed value is put in the passed position
   * 
   * @param value The value to set
   * @param pos The position to put the value into
   * @return a new SRow where with the value in the position
   */
  def set(value: Int, pos: Int): SRow = set(Some(value),pos)
  
  /**
   * Build a new SRow where the cell at the given position has been cleared
   * 
   * @param pos The position of the cell to clear
   * @return a new SRow where the value of the cell at the given position has been cleared
   */
  def unset(pos: Int) = set(None,pos)
  
  /**
   * Return a new row where the passed value is put in the passed position
   * 
   * @param value The value to set
   * @param pos The position to put the value into
   * @return a new SRow where with the value in the position
   */
  def set(value: Option[Int], pos: Int): SRow = {
    value.foreach(v => require(v >=SRow.minAllowedValue && v<=SRow.maxAllowedValue,s"Value $v of range"))
    require(isUnset(pos), s"Position $pos already contain a value $vals(pos)")
    
    val newValues: Vector[Option[Int]] = (0 to 8).foldLeft( List.empty[Option[Int]] )( (l,i) => {
      if (pos==i) value::l
      else vals(i)::l
    }).reverse.toVector
    new SRow(newValues)
  }
  
  /**
   * @param the position of the value to get
   * @return The value at the given position, if defined; empty otherwise
   */
  def get(pos: Int): Option[Int] = {
    require(pos>=0 && pos<=8,s"Wrong position $pos requested")
    vals(pos)
  }
  
  /**
   * Check if the settiung the value in the passed position is allowed or introduces an error.
   * This method delegates to SRow.containsDuplicates after building a new row that 
   * contains the passed value in the passed position.
   * 
   * An error means that the value is already present in the line
   * 
   * @param pos The position to check
   * @param the The value to check 
   * @return True if setting the value in the passed position does nt intorduce an errorl;
   *         False otherwise
   */
  def isAllowed(pos: Int, value: Int): Boolean = {
    require(Option(pos).isDefined,"Undefined pos")
    require(pos>=0 && pos<SRow.rowSize,s"Invalid position $pos")
    require(Option(value).isDefined, "Undefined value")
    require(value>=SRow.minAllowedValue && value<=SRow.maxAllowedValue,s"Ivalid value $value")
    
    if (vals(pos).isEmpty) {
      // There is no value in the passed position so we can check if this row already contains
      // the passed value
      numsSet.contains(value)
    } else {
      // The row already contains a value in the passed position so we must build a new row 
      // with the passed value in the passed position and check if it contains duplicates
      set(value,pos).containsDuplicates
    }
  }
  
  /** Friendly prints this row */
  override def toString(): String = {
    val temp = vals.map( v => {
      v match {
        case None => "-"
        case Some(n) => ""+n
      }
    })
    s"[${temp.mkString(" ")}]"
  }
}

object SRow {
  val minAllowedValue = 1
  val maxAllowedValue= 9
  val rowSize = 9
  
  /**
   * Builds a new SRow from a list of (position,value)
   * 
   * The position must be in [0, 8]
   * The value must be in [1, 9]
   */
  def apply(cells: List[(Int,Int)]): SRow = {
    require(Option(cells).isDefined)
    require(cells.size<=rowSize,s"Invalid size of row $cells.size")
    
    // Check that all the positions are in the proper range
    require(!cells.exists( v => v._1<0 || v._1>8),"Invalid position found")
    // Check that all the values are in the proper range
    require(!cells.exists( v => v._2<1 || v._2>9),"Invalid value found")
    
    val sortedCells: List[(Int,Int)] = cells.sortBy(_._1)
    
    val values = (0 to 8).foldLeft( List.empty[Option[Int]]) ( (l,p) => {
      val couple: Option[(Int,Int)] = cells.find( _._1==p)
      couple.map(_._2)::l
    }).reverse.toVector
    new SRow(values)
  }
  
  /**
   * Factory method to build a Row from a list of integers where 0
   * represent an undefined value
   * 
   * The purpose of this factory method is to ease the first initialization of a row
   */
  def apply(values: Vector[Int]): SRow = {
    require(Option(values).isDefined && ! values.isEmpty,"Invalid empty list of values")
    require(values.size==9,s"Wrong size ${values.size} of vector of values $values")
    require(!values.exists(n => n<0 || n>9),s"Number out of range in $values")
    
    val cells: List[Option[Int]] = values.map( n => {
      if (n==0) None
      else Some(n)
    }).toList
    new SRow(cells.toVector)
  }
  
  /**
   * Factory method to create an empty row
   */
  def apply(): SRow = apply(Vector(0,0,0,0,0,0,0,0,0))
  
}
