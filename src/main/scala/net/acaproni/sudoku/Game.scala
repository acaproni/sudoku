package net.acaproni.sudoku

import java.util.Scanner
import net.acaproni.sudoku.solver.EnrichedField
import scala.io.Source
import scala.io.BufferedSource
import net.acaproni.sudoku.solver.BasicSolver
import scala.collection.mutable.ListBuffer

/** 
 *  A sudoku game
 *  
 *  The game begins with a field filled with initial values and find the solution
 *  At the present this is not yet a game but a Sudoku solver.
 *  
 *  TODO: change the game allowing the user to play and provide the solver as an option
 *  
 *  @param rows The rows of the initial game
 */
class Game(rows: Vector[Vector[Int]]) {
  
  val rowsOfField = rows.map(r => SRow(r))
  val initialField = new Field(rowsOfField)
  
  /** All the steps applied to solve the game */
  val steps: ListBuffer[Field] = new ListBuffer()
  steps+=initialField
  
  var updated=false
  do {
    val initial = steps.last
    val solver = new BasicSolver(initial)
    val newField: Option[Field] = solver.solve
    if (newField.isDefined) {
      steps+=newField.get
    }
    updated = newField.isDefined
    println(s"Updated $updated; items list ${steps.size}")
  } while(updated)
}

object Game {
  def main(args: Array[String]): Unit = {
    println("Started")
    
    val f = Field()
    
    // Read the rows of the field from the console or the passed file
    val rows: Vector[Vector[Int]] = 
      if (args.length==0) Game.readFieldFromConsole()
      else Game.readFieldFromFile(args(0))
    
    val game = new Game(rows)
  }
  
  /**
   * read the initial field from the console
   * 
   * The user inserts one line at a time.
   * Each line is composed of the digits of the initial field eventually separated by spaces.
   * 
   * No other character is allowed.
   */
  def readFieldFromConsole(): Vector[Vector[Int]] = {
    val rows: Array[Vector[Int]] = new Array(9)
    
    val scanner = new Scanner(System.in)
    for (row <- 0 until SRow.rowSize) {
      println(s"Enter numbers of row ${row+1} (9 digits (0 is empty):")
      val str = scanner.nextLine()
      val digits = Game.processLine(str)
      println(s"digits = $digits")
      rows(row)=digits
    }
    rows.toVector
  }
  
  /**
   * Extracts the digits in the passed string
   * 
   * @param line the string to process
   * @return the digits in the line
   */
  def processLine(line: String): Vector[Int] = {
    // Check for correctness
      val charsInLine = for (
          t <- 0 until line.length();
          c = line.charAt(t)) yield (c)
        
      println(s"charsInLine = $charsInLine")
      // Ensure that all the values in the string are allowed
      require(charsInLine.forall(x => x==' ' || (x>='0'&& x<='9')),s"Invalid char in $line")
      // Ensure that the user provided exactly 9 digits
      require(charsInLine.count(x => x>='0'&& x<='9')==SRow.rowSize, s"Invalid number of digits in row $line")
      
      // Convert and return the digits in a vector of Int
      charsInLine.foldLeft (Vector.empty[Int]) ( (z,c) => {
        if (c==' ') z
        else {
          val value: Int = (""+c).toInt
          z.appended(value)
        }
      })
  }
  
  def readFieldFromFile(fileName: String): Vector[Vector[Int]] = {
    require(Option(fileName).isDefined && !fileName.isEmpty(),"Invalid empty file name")
    println(s"Processing $fileName")
    
    val bufferedSource: BufferedSource = Source.fromFile(fileName)
    println("File opened. raqding content...")
    val lines = bufferedSource.getLines()
    println(s"Content of $fileName read")
    
    //require(lines.size==9,s"Invalid number of lines (${lines.length}) in the file $fileName")
    
    //println(s"Read ${lines.size} lines from $fileName")
    
    val rows: Array[Vector[Int]] = new Array(9)
    
    for (row <- 0 until SRow.rowSize) {
      println(s"Processing row ${row+1}")
      val str = lines.next()
      val digits = Game.processLine(str)
      println(s"digits = $digits")
      rows(row)=digits
    }
    
    bufferedSource.close()
    
    rows.toVector
  }
  
}